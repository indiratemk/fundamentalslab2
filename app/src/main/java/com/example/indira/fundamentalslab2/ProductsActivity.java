package com.example.indira.fundamentalslab2;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class ProductsActivity extends AppCompatActivity {
    public static final String EXTRA_PRODUCT = "com.example.indira.fundamentalslab2.extra.PRODUCT";

    private Intent mIntent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_products);

        mIntent = getIntent();
    }

    public void appleClicked(View view) {
        mIntent.putExtra(EXTRA_PRODUCT, "apple");
        setResult(RESULT_OK, mIntent);
        finish();
    }

    public void bedClicked(View view) {
        mIntent.putExtra(EXTRA_PRODUCT, "bed");
        setResult(RESULT_OK, mIntent);
        finish();
    }

    public void tableClicked(View view) {
        mIntent.putExtra(EXTRA_PRODUCT, "table");
        setResult(RESULT_OK, mIntent);
        finish();
    }

    public void penClicked(View view) {
        mIntent.putExtra(EXTRA_PRODUCT, "pen");
        setResult(RESULT_OK, mIntent);
        finish();
    }

    public void breadClicked(View view) {
        mIntent.putExtra(EXTRA_PRODUCT, "bread");
        setResult(RESULT_OK, mIntent);
        finish();
    }

    public void bookClicked(View view) {
        mIntent.putExtra(EXTRA_PRODUCT, "book");
        setResult(RESULT_OK, mIntent);
        finish();
    }

    public void juiceClicked(View view) {
        mIntent.putExtra(EXTRA_PRODUCT, "juice");
        setResult(RESULT_OK, mIntent);
        finish();
    }

    public void milkClicked(View view) {
        mIntent.putExtra(EXTRA_PRODUCT, "milk");
        setResult(RESULT_OK, mIntent);
        finish();
    }

    public void orangeClicked(View view) {
        mIntent.putExtra(EXTRA_PRODUCT, "orange");
        setResult(RESULT_OK, mIntent);
        finish();
    }

    public void bananClicked(View view) {
        mIntent.putExtra(EXTRA_PRODUCT, "banan");
        setResult(RESULT_OK, mIntent);
        finish();
    }
}
