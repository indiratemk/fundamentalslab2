package com.example.indira.fundamentalslab2;

import android.content.Intent;
import android.net.Uri;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class ShoppingListActivity extends AppCompatActivity {

    public static final int REQUEST_CODE = 1;

    private Button mAddItemButton;
    private TextView mTextView;
    private EditText mShopTitleEditText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_shopping_list);

        mShopTitleEditText = findViewById(R.id.edit_text_shop_title);

        mTextView = findViewById(R.id.item1);
        mAddItemButton = findViewById(R.id.add_item_button);
        mAddItemButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ShoppingListActivity.this, ProductsActivity.class);
                startActivityForResult(intent, REQUEST_CODE);
            }
        });

        if (savedInstanceState != null) {
            mTextView.setText(savedInstanceState.getString("item"));
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_CODE && resultCode == RESULT_OK) {
            mTextView.setText(data.getStringExtra(ProductsActivity.EXTRA_PRODUCT));
        }
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putString("item", mTextView.getText().toString());
    }

    public void redirectToMap(View view) {
        String loc = mShopTitleEditText.getText().toString();
        Uri addressUri = Uri.parse("geo:0,0?q=" + loc);
        Intent intent = new Intent(Intent.ACTION_VIEW, addressUri);
        if (intent.resolveActivity(getPackageManager()) != null) {
            startActivity(intent);
        } else {
            Log.d("ShopTitle", "Can't handle this intent!");
        }
    }
}
