package com.example.indira.fundamentalslab2;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

public class ButtonsActivity extends AppCompatActivity {

    public static final String EXTRA_BUTTON1 =
            "com.example.indira.fundamentalslab2.extra.BUTTON1";
    public static final String EXTRA_BUTTON2 =
            "com.example.indira.fundamentalslab2.extra.BUTTON2";
    public static final String EXTRA_BUTTON3 =
            "com.example.indira.fundamentalslab2.extra.BUTTON3";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_buttons);
    }

    public void firstButtonPressed(View view) {
        Intent intent = new Intent(this, IdentifyButtonActivity.class);
        intent.putExtra(EXTRA_BUTTON1, "Button 1 pressed");
        startActivity(intent);
    }

    public void secondButtonPressed(View view) {
        Intent intent = new Intent(this, IdentifyButtonActivity.class);
        intent.putExtra(EXTRA_BUTTON2, "Button 2 pressed");
        startActivity(intent);
    }

    public void thirdButtonPressed(View view) {
        Intent intent = new Intent(this, IdentifyButtonActivity.class);
        intent.putExtra(EXTRA_BUTTON3, "Button 3 pressed");
        startActivity(intent);
    }
}
