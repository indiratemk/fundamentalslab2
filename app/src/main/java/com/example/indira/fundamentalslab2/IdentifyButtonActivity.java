package com.example.indira.fundamentalslab2;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.Button;
import android.widget.TextView;

public class IdentifyButtonActivity extends AppCompatActivity {
    private TextView mTextView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_identify_button);

        mTextView = findViewById(R.id.textView);

        Intent intent = getIntent();
        if (intent.hasExtra(ButtonsActivity.EXTRA_BUTTON1)) {
            mTextView.setText(intent.getStringExtra(ButtonsActivity.EXTRA_BUTTON1));
        } else if (intent.hasExtra(ButtonsActivity.EXTRA_BUTTON2)){
            mTextView.setText(intent.getStringExtra(ButtonsActivity.EXTRA_BUTTON2));
        } else if (intent.hasExtra(ButtonsActivity.EXTRA_BUTTON3)){
            mTextView.setText(intent.getStringExtra(ButtonsActivity.EXTRA_BUTTON3));
        }

    }
}
