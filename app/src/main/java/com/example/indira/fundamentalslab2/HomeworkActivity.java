package com.example.indira.fundamentalslab2;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

public class HomeworkActivity extends AppCompatActivity {

    private TextView mCountTextView;
    private int mCount = 0;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_homework);

        mCountTextView = findViewById(R.id.textView_count);

        if (savedInstanceState != null) {
            mCount = savedInstanceState.getInt("count");
            mCountTextView.setText(String.valueOf(mCount));
        }
    }

    public void increment(View view) {
        ++mCount;
        mCountTextView.setText(String.valueOf(mCount));
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        if (mCount != 0) {
            outState.putInt("count", mCount);
        }
    }
}
